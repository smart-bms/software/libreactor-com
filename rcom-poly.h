/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include <stdint.h>

#define RCOM_POLY_DEGREE 3

struct rcom_poly
{
    uint32_t coeffs[RCOM_POLY_DEGREE + 1];
};

void rcom_poly_init_float (struct rcom_poly *poly,
			   float *coeffs);
float rcom_poly_coeff_float (struct rcom_poly *poly,
				 int index);
uint32_t rcom_poly_compute (struct rcom_poly *poly,
			    uint32_t value);
