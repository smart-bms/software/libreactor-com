/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "rcom-serial.h"

#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

enum
{
    CMD_0,
    CMD_PING,
    CMD_TIMESTAMP,
    CMD_MESSAGE,
    CMD_VOLTAGE,
    CMD_CURRENT,
    CMD_STATUS,
    CMD_PROTECTION,
    CMD_BALANCING,
    CMD_TEMPERATURE,
    CMD_CONFIG,
    CMD_CAPLIMIT,
    CMD_QUERY,
    CMD_SET,
    CMD_RESET,
    CMD_REGREAD,
    CMD_REGWRITE,
};

static uint8_t crc_update(uint8_t crc, uint8_t data);
static int8_t send_frame (struct rcom *com,
			  uint8_t cmd,
			  const void *data,
			  uint16_t size);
static int8_t send_frame_full (struct rcom *com,
			       uint8_t cmd,
			       uint8_t array_c,
			       const void **data_v,
			       uint16_t *size_v);
static void feed_byte (struct rcom_serial *serial,
		       uint8_t byte);
static void parse_message (struct rcom_serial *serial);
static int8_t ping (struct rcom *com,
		    uint8_t ack);
static int8_t timestamp (struct rcom *com,
			 int64_t timestamp);
static int8_t message (struct rcom *com,
		       const char *msg);
static int8_t voltage (struct rcom *com,
		       const int16_t *cells,
		       uint8_t count);
static int8_t current (struct rcom *com,
		       int32_t current);
static int8_t status (struct rcom *com,
		      uint32_t status);
static int8_t protection (struct rcom *com,
			  uint32_t protection);
static int8_t balancing (struct rcom *com,
			 uint32_t balancing);
static int8_t temperature (struct rcom *com,
			   const int16_t *temperatures,
			   uint8_t count);
static int8_t config (struct rcom *com,
		      const struct rcom_conf *config);
static int8_t caplimit (struct rcom *com,
			uint8_t limit);
static int8_t query (struct rcom *com,
		     uint32_t query);
static int8_t set (struct rcom *com,
		   uint32_t status);
static int8_t reset (struct rcom *com,
		     uint32_t status);
static int8_t regread (struct rcom *com,
		       uint16_t address,
		       uint8_t size);
static int8_t regwrite (struct rcom *com,
			uint16_t address,
			uint8_t size,
			const uint8_t *data);

static struct rcom_api api = {
    .ping = ping,
    .timestamp = timestamp,
    .message = message,
    .voltage = voltage,
    .current = current,
    .status = status,
    .protection = protection,
    .balancing = balancing,
    .temperature = temperature,
    .config = config,
    .caplimit = caplimit,
    .query = query,
    .set = set,
    .reset = reset,
    .regread = regread,
    .regwrite = regwrite,
};

struct rcom *
rcom_serial_init (struct rcom_serial *serial,
		  rcom_serial_write_t write,
		  struct rcom *receiver)
{
    rcom_init (&serial->base, &api, receiver);

    serial->write = write;

    return &serial->base;
}

int16_t
rcom_serial_feed (struct rcom_serial *serial,
		  const void *data,
		  uint16_t bytes)
{
    uint16_t i;

    for (i = 0; i < bytes; i++) {
	feed_byte (serial, ((const uint8_t *) data)[i]);
    }

    return bytes;
}

static uint8_t
crc_update(uint8_t crc, uint8_t data)
{
    uint8_t i;

    crc ^= data;

    for (i = 0; i < 8; i++) {
	if (crc & 0x80)
	    crc = (crc << 1) ^ 0x07;
	else
	    crc = crc << 1;
    }

    return crc;
}

static int8_t send_frame (struct rcom *com,
			  uint8_t cmd,
			  const void *data,
			  uint16_t size)
{
    return send_frame_full (com, cmd, 1, &data, &size);
}

static int8_t
send_frame_full (struct rcom *com,
		 uint8_t cmd,
		 uint8_t array_c,
		 const void **data_v,
		 uint16_t *size_v)
{
    struct rcom_serial *serial;
    uint8_t zero[4], header[8], fuse[2], crc, i, k;
    uint16_t size_total, written;

    serial = (struct rcom_serial *) com;
    memset (zero, 0, sizeof (zero));
    written = serial->write (serial, zero, sizeof (zero));

    if (written < sizeof (zero)) {
	return -1;
    }

    size_total = 0;

    for (i = 0; i < array_c; i++) {
	size_total += size_v[i];
    }

    crc = cmd;
    crc = crc_update (crc, size_total);
    crc = crc_update (crc, 0);
    crc = crc_update (crc, 0);

    header[0] = 0xff;
    header[1] = 0x00;
    header[2] = cmd;
    header[3] = size_total;
    header[4] = 0x00;
    header[5] = 0x00;
    header[6] = 0xaa;
    header[7] = crc;

    written = serial->write (serial, header, sizeof (header));

    if (written < sizeof (header)) {
	return -1;
    }

    for (i = 0; i < array_c; i++) {
	written = serial->write (serial, data_v[i], size_v[i]);

	if (written < size_v[i]) {
	    return -1;
	}

	for (k = 0; k < size_v[i]; k++) {
	    crc = crc_update (crc, ((const uint8_t *) data_v[i])[k]);
	}
    }

    fuse[0] = crc;
    fuse[1] = 0x55;

    written = serial->write (serial, fuse, sizeof (fuse));

    if (written < sizeof (fuse)) {
	return -1;
    }

    return 0;
}

static void
feed_byte (struct rcom_serial *serial,
	   uint8_t byte)
{
    serial->cmd_index++;

    switch (serial->cmd_index) {
    case 1:
	if (byte != 0xff) {
	    serial->cmd_index = 0;
	}
	break;

    case 2:
	if (byte != 0x00) {
	    serial->cmd_index = 0;
	}
	break;

    case 3:
	serial->cmd_code = byte;
	serial->cmd_crc = byte;
	break;

    case 4:
	serial->cmd_size = byte;
	serial->cmd_crc = crc_update (serial->cmd_crc, byte);
	break;

    case 5:
    case 6:
	serial->cmd_crc = crc_update (serial->cmd_crc, byte);
	break;

    case 7:
	if (byte != 0xaa) {
	    serial->cmd_index = 0;
	}
	break;

    case 8:
	if (byte != serial->cmd_crc) {
	    serial->cmd_index = 0;
	}
	break;

    case 9:
	serial->cmd_bytes = 0;
	/* fall through */

    default:
	if (serial->cmd_index < serial->cmd_size + 9) {
	    serial->cmd_data[serial->cmd_bytes++] = byte;
	    serial->cmd_crc = crc_update (serial->cmd_crc, byte);
	} else if (serial->cmd_index == serial->cmd_size + 9) {
	    if (byte != serial->cmd_crc) {
		printf ("bad crc\n");

		serial->cmd_index = 0;
	    }
	} else if (byte != 0x55) {
	    printf ("bad fuse\n");
	    serial->cmd_index = 0;
	} else {
	    parse_message (serial);
	    serial->cmd_index = 0;
	}

    }
}

static void
parse_message (struct rcom_serial *serial)
{
    struct rcom *receiver;
    const void *data;
    uint8_t size;

    receiver = serial->base.receiver;
    data = serial->cmd_data;
    size = serial->cmd_bytes;

    switch (serial->cmd_code) {
    case CMD_PING:
	rcom_ping (receiver, *(const uint8_t *) data);
	break;

    case CMD_TIMESTAMP:
	rcom_timestamp (receiver, *(const int64_t *) data);
	break;

    case CMD_MESSAGE:
	rcom_message (receiver, data);
	break;

    case CMD_VOLTAGE:
	rcom_voltage (receiver, data, size / sizeof (int16_t));
	break;

    case CMD_CURRENT:
	rcom_current (receiver, *(const int32_t *) data);
	break;

    case CMD_STATUS:
	rcom_status (receiver, *(const uint32_t *) data);
	break;

    case CMD_PROTECTION:
	rcom_protection (receiver, *(const uint32_t *) data);
	break;

    case CMD_BALANCING:
	rcom_balancing (receiver, *(const uint32_t *) data);
	break;

    case CMD_TEMPERATURE:
	rcom_temperature (receiver, data, size / sizeof (int16_t));
	break;

    case CMD_CONFIG:
	rcom_config (receiver, data);
	break;

    case CMD_CAPLIMIT:
	rcom_caplimit (receiver, *(const uint8_t *) data);
	break;

    case CMD_QUERY:
	rcom_query (receiver, *(const uint32_t *) data);
	break;
	
    case CMD_SET:
	rcom_set (receiver, *(const uint32_t *) data);
	break;

    case CMD_RESET:
	rcom_set (receiver, *(const uint32_t *) data);
	break;

    case CMD_REGREAD:
	rcom_regread (receiver,
		      ((const uint16_t *) data)[0],
		      ((const uint8_t *) data)[2]);
	break;

    case CMD_REGWRITE:
	rcom_regwrite (receiver,
		       ((const uint16_t *) data)[0],
		       ((const uint8_t *) data)[2],
		       (const uint8_t *) data + 3);
	break;
    }
}

static int8_t
ping (struct rcom *com,
      uint8_t ack)
{
    return send_frame (com, CMD_PING, &ack, sizeof (ack));
}

static int8_t
timestamp (struct rcom *com,
	   int64_t timestamp)
{
    return send_frame (com, CMD_TIMESTAMP, &timestamp, sizeof (timestamp));
}

static int8_t
message (struct rcom *com,
	 const char *msg)
{
    return send_frame (com, CMD_MESSAGE, msg, strlen (msg) + 1);
}

static int8_t
voltage (struct rcom *com,
	 const int16_t *cells,
	 uint8_t count)
{
    return send_frame (com, CMD_VOLTAGE, cells, count * sizeof (int16_t));
}

static int8_t
current (struct rcom *com,
	 int32_t current)
{
    return send_frame (com, CMD_CURRENT, &current, sizeof (int32_t));
}

static int8_t
status (struct rcom *com,
	uint32_t status)
{
    return send_frame (com, CMD_STATUS, &status, sizeof (uint32_t));
}

static int8_t
protection (struct rcom *com,
	    uint32_t protection)
{
    return send_frame (com, CMD_PROTECTION, &protection, sizeof (uint32_t));
}

static int8_t
balancing (struct rcom *com,
	   uint32_t balancing)
{
    return send_frame (com, CMD_BALANCING, &balancing, sizeof (uint32_t));
}

static int8_t
temperature (struct rcom *com,
	     const int16_t *temperatures,
	     uint8_t count)
{
    return send_frame (com, CMD_TEMPERATURE, temperatures, count * sizeof (int16_t));
}

static int8_t
config (struct rcom *com,
	const struct rcom_conf *config)
{
    return send_frame (com, CMD_CONFIG, config, sizeof (struct rcom_conf));
}

static int8_t
caplimit (struct rcom *com,
	  uint8_t limit)
{
    return send_frame (com, CMD_CAPLIMIT, &limit, sizeof (limit));
}

static int8_t
query (struct rcom *com,
       uint32_t query)
{
    return send_frame (com, CMD_QUERY, &query, sizeof (query));
}

static int8_t
set (struct rcom *com,
     uint32_t status)
{
    return send_frame (com, CMD_SET, &status, sizeof (status));
}

static int8_t
reset (struct rcom *com,
       uint32_t status)
{
    return send_frame (com, CMD_RESET, &status, sizeof (status));
}

static int8_t
regread (struct rcom *com,
	 uint16_t address,
	 uint8_t size)
{
    uint8_t buff[3];

    memcpy (buff + 0, &address, 2);
    memcpy (buff + 2, &size, 1);

    return send_frame (com, CMD_REGREAD, buff, 3);
}

static int8_t
regwrite (struct rcom *com,
	  uint16_t address,
	  uint8_t size,
	  const uint8_t *data)
{
    const void *data_v[] = {
	&address,
	&size,
	data,
    };

    uint16_t size_v[] = {
	2, 1, size,
    };

    return send_frame_full (com, CMD_REGWRITE, 3,
			    data_v, size_v);
}
