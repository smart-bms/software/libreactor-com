/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "rcom-conf.h"

#define RCOM_STATUS_CHARGING		1
#define RCOM_STATUS_DISCHARGING		2
#define RCOM_STATUS_PROTECTED		4
#define RCOM_STATUS_LOCKED		8

#define RCOM_PROTECTION_CELL_UV		1
#define RCOM_PROTECTION_CELL_OV		2
#define RCOM_PROTECTION_CHARGING_OT	4
#define RCOM_PROTECTION_CHARGING_UT	8
#define RCOM_PROTECTION_DISCHARGING_OT	16
#define RCOM_PROTECTION_DISCHARGING_UT	32
#define RCOM_PROTECTION_CHARGING_OC	64
#define RCOM_PROTECTION_DISCHARGING_OC	128

#define RCOM_QUERY_TIMESTAMP		1
#define RCOM_QUERY_VOLTAGE		2
#define RCOM_QUERY_CURRENT		4
#define RCOM_QUERY_STATUS		8
#define RCOM_QUERY_PROTECTION		16
#define RCOM_QUERY_BALANCING		32
#define RCOM_QUERY_TEMPERATURE		64
#define RCOM_QUERY_CONFIG		128
#define RCOM_QUERY_CAPLIMIT		256

struct rcom
{
    struct rcom_api *api;
    struct rcom *receiver;
};

struct rcom_api
{
    int8_t  (*ping) (struct rcom *com,
		     uint8_t ack);
    int8_t  (*timestamp) (struct rcom *com,
			  int64_t time);
    int8_t  (*message) (struct rcom *com,
			const char *message);
    int8_t  (*voltage) (struct rcom *com,
			const int16_t *cells,
			uint8_t count);
    int8_t  (*current) (struct rcom *com,
			int32_t current);
    int8_t  (*status) (struct rcom *com,
		       uint32_t status);
    int8_t  (*protection) (struct rcom *com,
			   uint32_t protection);
    int8_t  (*balancing) (struct rcom *com,
			  uint32_t balancing);
    int8_t  (*temperature) (struct rcom *com,
			    const int16_t *temperatures,
			    uint8_t count);
    int8_t  (*config) (struct rcom *com,
		       const struct rcom_conf *config);
    int8_t  (*caplimit)	(struct rcom *com,
			 uint8_t limit);
    int8_t  (*query) (struct rcom *com,
		      uint32_t query);
    int8_t  (*set) (struct rcom *com,
		    uint32_t status);
    int8_t  (*reset) (struct rcom *com,
		      uint32_t status);
    int8_t  (*regread) (struct rcom *com,
			uint16_t address,
			uint8_t size);
    int8_t  (*regwrite) (struct rcom *com,
			 uint16_t address,
			 uint8_t size,
			 const uint8_t *data);
};

extern struct rcom *rcom_default;

void rcom_default_fault ();

void rcom_init (struct rcom *com,
		struct rcom_api *api,
		struct rcom *receiver);
int8_t rcom_ping (struct rcom *com,
		  uint8_t ack);
int8_t rcom_timestamp (struct rcom *com,
		       int64_t timestamp);
int8_t rcom_message (struct rcom *com,
		     const char *message);
int8_t rcom_message_format (struct rcom *com,
			    const char *fmt,
			    ...) __attribute__((format (printf, 2, 3)));
int8_t rcom_voltage (struct rcom *com,
		     const int16_t *cells,
		     uint8_t count);
int8_t rcom_current (struct rcom *com,
		     int32_t current);
int8_t rcom_status (struct rcom *com,
		    uint32_t status);
int8_t rcom_protection (struct rcom *com,
			uint32_t protection);
int8_t rcom_balancing (struct rcom *com,
		       uint32_t balancing);
int8_t rcom_temperature (struct rcom *com,
			 const int16_t *temperatures,
			 uint8_t count);
int8_t rcom_config (struct rcom *com,
		    const struct rcom_conf *config);
int8_t rcom_caplimit (struct rcom *com,
		      uint8_t limit);
int8_t rcom_query (struct rcom *com,
		   uint32_t query);
int8_t rcom_set (struct rcom *com,
		 uint32_t status);
int8_t rcom_reset (struct rcom *com,
		   uint32_t status);
int8_t rcom_regread (struct rcom *com,
		     uint16_t address,
		     uint8_t size);
int8_t rcom_regwrite (struct rcom *com,
		      uint16_t address,
		      uint8_t size,
		      const uint8_t *data);
