/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "rcom-poly.h"

struct rcom_conf
{
    uint16_t cell_ov;
    uint16_t cell_ov_release;
    uint16_t cell_uv;
    uint16_t cell_uv_release;
    uint32_t chg_current_limit;
    uint32_t dsg_current_limit;
    uint16_t chg_current_delay;
    uint16_t dsg_current_delay;
    uint16_t chg_temperature_min;
    uint16_t chg_temperature_max;
    uint16_t dsg_temperature_min;
    uint16_t dsg_temperature_max;
    uint32_t chg_current_max;
    uint32_t dsg_current_max;
    uint32_t nominal_capacity;
    uint32_t nominal_voltage;
    struct rcom_poly capacity_polynomial;
    struct rcom_poly vitality_polynomial;
    uint8_t capacity_limit_min;
    uint8_t capacity_limit_max;
} __attribute__((packed));

