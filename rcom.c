/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "rcom.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

#define VCALL(name, ...) \
    do { \
	struct rcom *cc; \
	if (com != NULL) { \
	    cc = com; \
	} else { \
	    cc = rcom_default; \
	    if (cc == NULL) { \
		rcom_default_fault (); \
		return -1; \
	    } \
       	} \
	if (cc->api->name != (void *) 0) { \
	    return cc->api->name (cc, ##__VA_ARGS__); \
	} else { \
	    return -1; \
	} \
    } while (0)

struct rcom *rcom_default;

__attribute__((weak))
void rcom_default_fault ()
{}

void
rcom_init (struct rcom *com,
	   struct rcom_api *api,
	   struct rcom *receiver)
{
    com->api = api;
    com->receiver = receiver;
}

int8_t
rcom_ping (struct rcom *com,
	   uint8_t ack)
{
    VCALL (ping, ack);
}

int8_t
rcom_timestamp (struct rcom *com,
		int64_t time)
{
    VCALL (timestamp, time);
}

int8_t
rcom_message (struct rcom *com,
	      const char *msg)
{
    VCALL (message, msg);
}

int8_t
rcom_message_format (struct rcom *com,
		     const char *fmt,
		     ...)
{
    char buffer[80];
    va_list args;

    va_start (args, fmt);
    vsnprintf (buffer, sizeof (buffer), fmt, args);
    va_end (args);

    return rcom_message (com, buffer);
}

int8_t
rcom_voltage (struct rcom *com,
	      const int16_t *cells,
	      uint8_t count)
{
    VCALL (voltage, cells, count);
}

int8_t
rcom_current (struct rcom *com,
	      int32_t current)
{
    VCALL (current, current);
}

int8_t
rcom_status (struct rcom *com,
	     uint32_t status)
{
    VCALL (status, status);
}

int8_t
rcom_protection (struct rcom *com,
		 uint32_t protection)
{
    VCALL (protection, protection);
}

int8_t
rcom_balancing (struct rcom *com,
		uint32_t balancing)
{
    VCALL (balancing, balancing);
}

int8_t
rcom_temperature (struct rcom *com,
		  const int16_t *temperatures,
		  uint8_t count)
{
    VCALL (temperature, temperatures, count);
}

int8_t
rcom_config (struct rcom *com,
	     const struct rcom_conf *config)
{
    VCALL (config, config);
}

int8_t
rcom_caplimit (struct rcom *com,
	       uint8_t limit)
{
    VCALL (caplimit, limit);
}

int8_t
rcom_query (struct rcom *com,
	    uint32_t query)
{
    VCALL (query, query);
}

int8_t
rcom_set (struct rcom *com,
	  uint32_t status)
{
    VCALL (set, status);
}

int8_t
rcom_reset (struct rcom *com,
	    uint32_t status)
{
    VCALL (reset, status);
}

int8_t
rcom_regread (struct rcom *com,
	      uint16_t address,
	      uint8_t size)
{
    VCALL (regread, address, size);
}

int8_t
rcom_regwrite (struct rcom *com,
	       uint16_t address,
	       uint8_t size,
	       const uint8_t *data)
{
    VCALL (regwrite, address, size, data);
}
