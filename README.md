# Reactor COM

This project aims to provide common and portable interface of communication
between Reactor BMS and other devices, mainly desktop application but
potentially any other device.

It's written in plain C using fixed sized primitive variables with minimum
dependency on standard libc. It provides universal structure and it's methods
to send and receive Reactor commands. By design, code should be easily portable
from tiny 8 bit AVR devices to desktop x64\_64 processors.

rcom.h defines abstract interface to create a node and transmit data.  
rcom-serial.h defines serial byte stream layer and frame packing algorithm, to
serialize Reactor commands into byte stream that can be send via anything.

It does not rely on any particular serial device implementation.
The user is responsible to point rcom\_serial\_write\_t function that
writes bytes from the stream, and push received bytes to rcom\_serial\_feed
method.

# gobject
gobject directory contains GObject based wrapper over structures described
above. It makes it easier to use reactor-com on higher level programming where
glib is available. The objects are scanned to generate .gir, .typelib and .vapi
files that makes it possible to use reactor-com directly from Python,
JavaScript or Vala, with no need to write extra glue code.
