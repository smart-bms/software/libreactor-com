# Copyright (c) 2020 Reactor Energy
# Mieszko Mazurek <mimaz@gmx.com>

project('reactor-com', 'c', version: '0.1')

sources = [
  'rcom.c',
  'rcom-serial.c',
  'rcom-poly.c',
]

headers = [
  'rcom.h',
  'rcom-serial.h',
  'rcom-poly.h',
  'rcom-conf.h',
]

header_subdir = 'reactor-energy'

reactor_com_lib = static_library(meson.project_name(),
  sources, install: true)

reactor_com_inc = include_directories('.')

reactor_com_dep = declare_dependency(link_with: reactor_com_lib,
  include_directories: reactor_com_inc)

install_headers(headers, subdir: header_subdir)

pkg = import('pkgconfig')
pkg.generate(reactor_com_lib,
  version: meson.project_version(),
  name: 'reactor-com',
  subdirs: header_subdir,
  description: 'The library that facilitates the communication ' +
	       'and protocol between a Reactor battery and a desktop client')

meson.override_dependency('reactor-com', reactor_com_dep)
