/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "rcom.h"

struct rcom_serial;

typedef int16_t (*rcom_serial_write_t) (struct rcom_serial *serial,
					const void *data,
					uint16_t bytes);

struct rcom_serial
{
    struct rcom		    base;
    rcom_serial_write_t	    write;
    uint8_t		    cmd_index;
    uint8_t		    cmd_code;
    uint8_t		    cmd_crc;
    uint8_t		    cmd_size;
    uint8_t		    cmd_bytes;
    uint8_t		    cmd_data[sizeof (struct rcom_conf)]
			    __attribute__((aligned (8)));
};

struct rcom *rcom_serial_init (struct rcom_serial *serial,
			       rcom_serial_write_t write,
			       struct rcom *receiver);
int16_t rcom_serial_feed (struct rcom_serial *serial,
			  const void *data,
			  uint16_t bytes);
