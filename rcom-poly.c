/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#include "rcom-poly.h"

#include <math.h>

#define BASE (1ll << 16)

void
rcom_poly_init_float (struct rcom_poly *poly,
		      float *coeffs)
{
    int i;

    for (i = 0; i <= RCOM_POLY_DEGREE; i++) {
	poly->coeffs[i] = coeffs[i] * BASE;
    }
}

float
rcom_poly_coeff_float (struct rcom_poly *poly,
		       int index)
{
    if (index <= RCOM_POLY_DEGREE) {
	return (float) poly->coeffs[index] / BASE;
    }

#ifdef NAN
    return NAN;
#else
    return 0;
#endif
}

static int64_t
fixed_divide_base (int64_t p, int64_t unit)
{
    int64_t q, r;

    q = p / BASE;
    r = p - q * BASE;

    if (r > BASE / 2) {
	q += unit;
    }

    return q;
}

uint32_t
rcom_poly_compute (struct rcom_poly *poly,
		   uint32_t value)
{
    int64_t result, term;
    int i, k;

    result = 0;

    for (i = 0; i <= RCOM_POLY_DEGREE; i++) {
	term = BASE;

	for (k = 0; k < i; k++) {
	    term *= value;
	}

	term *= poly->coeffs[i];
	term = fixed_divide_base (term, BASE);

	result += term;
    }

    return fixed_divide_base (result, 1);
}
